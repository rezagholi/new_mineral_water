
document.getElementById("submit-login").addEventListener("click",function(){
    if(!validateForm()){
        return false;
    }
    document.getElementsByClassName("login-input")[0].classList.remove("invalid");
    document.getElementById("reg-form").submit();
    return false;
});
function validate_input(value, pattern){
    return pattern.test(String(value).toLowerCase());
}


function validateForm() {
    // This function deals with validation of the form fields
    var reg_form, form_inputs, valid = true;
    var validation_object = {
         "phone_email": {
             "validation_pattern": [
              /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
             ,/^(09|9|\+98([\s]{1,3})?9)[0123][0-9]\d{7}$/],
             "error_message": "لطفا شماره موبایل یا ایمیل را به فرمت صحیح وارد کنید"
        },
        "password": {
            "error_message": "لطفا پسورد را با با طول 8 تا 12 کاراکتر انتخاب کنید",
            "validation_pattern": [/[a-zA-Z0-9]{8,12}$/],
        }
    };
    reg_form = document.getElementById("reg-form");
    form_inputs = reg_form.getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (var i = 0; i < form_inputs.length; i++) {
        var input_val = form_inputs[i].value;
        var validation_pattern = validation_object[form_inputs[i].dataset.type].validation_pattern;
        var validate = false;
        // if an input has mor than one pattern
        if (validation_pattern.length > 1){
            for(var index = 0; index < validation_pattern.length; index++){
                if (validate_input(input_val, validation_pattern[index])){
                    validate = true;
                }
            }
        }
        else{
            if (validate_input(input_val, validation_pattern[0])) {
                validate = true;
            }
        }

        // check if input is valid
        if (input_val == "" || validate == false){
            // add invalid class to input
            if (!(form_inputs[i].classList.contains("invalid"))) {
                form_inputs[i].className += " invalid";
                form_inputs[i].nextElementSibling.innerHTML = validation_object[form_inputs[i].dataset.type].error_message;
            }
            // and set the current valid status to false:
            valid = false;
        }
        else{
            form_inputs[i].nextElementSibling.innerHTML = "";
            form_inputs[i].classList.remove("invalid");
        }
    }
    return valid; // return the valid status
}


