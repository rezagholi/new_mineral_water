
// first selectors
const first_img = document.getElementById("stroke1");
const first_txt = document.getElementsByClassName("first-txt")[0];
const bottle_img = document.getElementById("one_half_liter");
const img_overlay = document.getElementsByClassName("img_overlay")[0];
// first gsap
const controller = new ScrollMagic.Controller();
const first_frame = gsap.timeline({ defaults: { duration: 0.8 }, paused: true })
    .to(img_overlay, { bottom: "-100%" })
    .from(first_txt, {y: -100, opacity: 0, duration: 0.6 });

// first product animation
new ScrollMagic.Scene({
    triggerElement: '#first-product',
    duration: 0,
    triggerHook: 0
})
    .on("enter", function (e) {
        first_frame.play();
    })
    .addTo(controller); 


// second product animation


// third product animation


// fourth product animation